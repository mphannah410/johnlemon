using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flashlight : MonoBehaviour
{

    // Image BatteryTimeFull;
    public bool isOn = false;
    public GameObject lightSource;
    public GameObject collider;
    public bool failSafe = false;
    public float battery = 20;
    public float maxBattery = 20;

    // void Start()
    // {
    //     BatteryTimeFull = GetComponent<Image> ();    
    // }

    void Update()
    {
        // BatteryTimeFull.fillAmount = battery / maxBattery;
        
        if (battery > 0)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (isOn == false && failSafe == false)
                {
                    failSafe = true;
                    lightSource.SetActive(true);
                    collider.SetActive(true);
                    isOn = true;
                    StartCoroutine(FailSafe());
                }
                if (isOn == true && failSafe == false)
                {
                    failSafe = true;
                    lightSource.SetActive(false);
                    collider.SetActive(false);
                    isOn = false;
                    StartCoroutine(FailSafe());
                }
            }
        }
        if (isOn == true)
        {
            battery = battery - Time.deltaTime;

            if (battery <= 0)
            {
                lightSource.SetActive(false);
                collider.SetActive(false);
                isOn = false;
            }
        }
    }

    IEnumerator FailSafe()
    {
        yield return new WaitForSeconds(0.25f);
        failSafe = false;
    }
}
