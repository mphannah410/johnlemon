using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryTime : MonoBehaviour
{

    Image batteryTime;
    private Flashlight flashLight_script;
    public GameObject flashLight;


    // Start is called before the first frame update
    void Start()
    {
        flashLight_script = flashLight.GetComponent<Flashlight> ();
        batteryTime = GetComponent<Image> ();
    }

    // Update is called once per frame
    void Update()
    {
        if (flashLight_script.isOn == true)
        {
            batteryTime.fillAmount = flashLight_script.battery / flashLight_script.maxBattery;
        }
    }
}
